var data = [{
    title: '1. Correctional System of the Philippines',
    topicId:'1',
    id: 1
}, {
    title: '2. Penal Management',
    topicId:'1',
    id: 2
}, {
    title: '3. Probation Law of the Philippines',
    topicId:'1',
    id: 3
}, {
    title: '1. Crime Detection',
    topicId:'2',
    id: 4
}, {
    title: '2. Special Crime Investigation',
    topicId:'2',
    id: 5
}, {
    title: '3. Traffic Operation and Accident Investigation',
    topicId:'2',
    id: 6
}, {
    title: '4. Drug Education and Vice Control',
    topicId:'2',
    id: 7
}, {
    title: '5. Police Report Writing',
    topicId:'2',
    id: 8
}, {
    title: '1. Introduction',
    topicId:'3',
    id: 9
}, {
    title: '2. Criminal law RPC(book 1)',
    topicId:'3',
    id: 10
}, {
    title: '3. Criminal law RPC(book 2)',
    topicId:'3',
    id: 11
}, {
    title: '4.Criminal Procedure',
    topicId:'3',
    id: 12
}, {
    title: '5. Evidence',
    topicId:'3',
    id: 13
},{
    title: '6. Definition of Terms',
    topicId:'3',
    id: 14
},{
    title: '1.Criminal Sociology',
    topicId:'4',
    id: 15
},{
    title: '2.Introduction to Criminology',
    topicId:'4',
    id: 16
},{
    title: '3.Juvenile Delinquency',
    topicId:'4',
    id: 17
},{
    title: '4.Human Behavior and Crises Management',
    topicId:'4',
    id: 18
},{
    title: '5.Seminar on Contemporary Police Problem',
    topicId:'4',
    id: 19
},{
    title: '6.Police Ethics and Community Relations',
    topicId:'4',
    id: 20
},{
    title: '1.Criminalistics',
    topicId:'5',
    id: 21
},{
    title: '2.Police Photography',
    topicId:'5',
    id: 22
},{
    title: '3.Personal Identification',
    topicId:'5',
    id: 23
},{
    title: '4.Forensic Medicine',
    topicId:'5',
    id: 24
},{
    title: '5.Lie Detection and Interrogation',
    topicId:'5',
    id: 25
},{
    title: '6.Forensic Ballistics',
    topicId:'5',
    id: 26
},{
    title: '7.Questioned Document',
    topicId:'5',
    id: 27
},{
    title: '8.Forensic Chemistry and Toxicology',
    topicId:'5',
    id: 28
},{
    title: '1.Law Enforcement Administration',
    topicId:'6',
    id: 29
},{
    title: '2.Administration of Police Organization',
    topicId:'6',
    id: 30
},{
    title: '3.Patrol Organization and Operation',
    topicId:'6',
    id: 31
},{
    title: '4.Police Operational Planning',
    topicId:'6',
    id: 32
},{
    title: '5.Intelligence and Secret Service',
    topicId:'6',
    id: 33
},{
    title: '6.Industrial Security Management',
    topicId:'6',
    id: 34
}];

crimapp.Values.value('Lessons', {
    lessons: data
});
