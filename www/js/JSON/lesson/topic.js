var data = [{
    title: 'Correctional Administration',
    id: 1
}, {
    title: 'Crime Detection',
    id: 2
}, {
    title: 'Criminal Jurisprudence and Procedure',
    id: 3
}, {
    title: 'Criminal Sociology',
    id: 4
}, {
    title: 'Criminalistics',
    id: 5
}, {
    title: 'Law Enforcement Administration',
    id: 6
}];

crimapp.Values.value('Topics', {
    topics: data
});
