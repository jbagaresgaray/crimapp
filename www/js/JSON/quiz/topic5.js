var data = [{
    topicId: 5,
    ans: 0,
    title: 'There is freehand invitation and is considered as the most skillful class of forgery',
    choices: [{
        choice: 'simulated or copied forgery',
        checked: false
    }, {
        choice: 'simple forgery',
        checked: false
    }, {
        choice: 'traced forgery',
        checked: false
    }, {
        choice: 'carbon tracing',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 2,
    title: 'Condensed and compact set of authentic specimen which is adequate and proper, should contain a cross section of the material from known sources.',
    choices: [{
        choice: 'disguised document',
        checked: false
    }, {
        choice: 'questioned document',
        checked: false
    }, {
        choice: 'standard document',
        checked: false
    }, {
        choice: 'requested document',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 2,
    title: 'Specimens of hand writing or of typescript which is of known origin.',
    choices: [{
        choice: 'Letters',
        checked: false
    }, {
        choice: 'Samples',
        checked: false
    }, {
        choice: 'Exemplars',
        checked: false
    }, {
        choice: 'Documents',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 3,
    title: 'A document which is being questioned because of its origin, its contents or the circumstances or the stories of its production.',
    choices: [{
        choice: 'disputed document',
        checked: false
    }, {
        choice: 'standard document',
        checked: false
    }, {
        choice: 'requested document',
        checked: false
    }, {
        choice: 'questioned document',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 1,
    title: 'The art of beautiful writing is known as',
    choices: [{
        choice: 'Drafting',
        checked: false
    }, {
        choice: 'Calligraphy',
        checked: false
    }, {
        choice: 'Art appreciation',
        checked: false
    }, {
        choice: 'Gothic',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 3,
    title: 'Any written instrument by which a right or obligation is established.',
    choices: [{
        choice: 'Certificate',
        checked: false
    }, {
        choice: 'Subpoena',
        checked: false
    }, {
        choice: 'Warrant',
        checked: false
    }, {
        choice: 'Document ',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 3,
    title: 'A type of fingerprint pattern in which the slope or downward flow of the innermost sufficient recurve is towards the thumb of radius   bone of the hand of origin.',
    choices: [{
        choice: 'ulnar loop ',
        checked: false
    }, {
        choice: 'tented arch',
        checked: false
    }, {
        choice: 'accidental whorl',
        checked: false
    }, {
        choice: 'radial loop',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 3,
    title: 'The forking or dividing of one line to two or more branches.',
    choices: [{
        choice: 'Ridge',
        checked: false
    }, {
        choice: 'Island',
        checked: false
    }, {
        choice: 'Delta',
        checked: false
    }, {
        choice: 'Bifurcation',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 2,
    title: 'The point on a ridge at or in front of and nearest the center of the divergence of the type lines.',
    choices: [{
        choice: 'Divergence',
        checked: false
    }, {
        choice: 'Island',
        checked: false
    }, {
        choice: 'Delta',
        checked: false
    }, {
        choice: 'Bifurcation',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 1,
    title: 'The following are considerations used for the identification of a loop except one:',
    choices: [{
        choice: 'Delta',
        checked: false
    }, {
        choice: 'Core',
        checked: false
    }, {
        choice: 'a sufficient recurve',
        checked: false
    }, {
        choice: 'a ridge count across a looping bridge',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 1,
    title: 'The process of recording fingerprint through the use of fingerprint ink.',
    choices: [{
        choice: 'Pathology',
        checked: false
    }, {
        choice: 'Fingerprinting',
        checked: false
    }, {
        choice: 'Dactyloscopy',
        checked: false
    }, {
        choice: 'Printing press',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 2,
    title: 'The fingerprint method of identification.',
    choices: [{
        choice: 'Pathology',
        checked: false
    }, {
        choice: 'Fingerprinting',
        checked: false
    }, {
        choice: 'Dactyloscopy',
        checked: false
    }, {
        choice: 'Printing press',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 2,
    title: 'Two lines that run parallel or nearly parallel, diverge and surround  the pattern area.',
    choices: [{
        choice: 'Ridges',
        checked: false
    }, {
        choice: 'Delta',
        checked: false
    }, {
        choice: 'Type line',
        checked: false
    }, {
        choice: 'Bifurcation',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 2,
    title: 'A part of the whorl or loop in which appear the cores, deltas and ridges.',
    choices: [{
        choice: 'type line',
        checked: false
    }, {
        choice: 'bifurcation',
        checked: false
    }, {
        choice: 'pattern area',
        checked: false
    }, {
        choice: 'furrow',
        checked: false
    }]
}, {
    topicId: 5,
    ans: 3,
    title: 'Fingerprints left on various surfaces at the crime scene which are not clearly visible.',
    choices: [{
        choice: 'plane impressions',
        checked: false
    }, {
        choice: 'visible fingerprints',
        checked: false
    }, {
        choice: 'rolled impressions',
        checked: false
    }, {
        choice: 'latent fingerprints',
        checked: false
    }]
}
];

crimapp.Values.value('Topic5', {
    quiz: data
});
