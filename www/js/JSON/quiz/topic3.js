var data = [{
    topicId: 3,
    ans: 2,
    title: 'A Branch of municipal law which defines crimes, treats of their nature and provides for their punishment.',
    choices: [{
        choice: 'Procedural Law',
        checked: false
    }, {
        choice: 'Civil Law',
        checked: false
    }, {
        choice: 'Criminal Law',
        checked: false
    }, {
        choice: 'Political Law',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 1,
    title: 'One of the following is not a characteristic of criminal law.',
    choices: [{
        choice: 'General',
        checked: false
    }, {
        choice: 'Territorial',
        checked: false
    }, {
        choice: 'Prospective',
        checked: false
    }, {
        choice: 'Retroactive',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 0,
    title: 'Criminal law is binding on all person who reside or sojourn in the Philippines. This characteristic of criminal law is known as',
    choices: [{
        choice: 'General',
        checked: false
    }, {
        choice: 'Territorial',
        checked: false
    }, {
        choice: 'Prospective',
        checked: false
    }, {
        choice: 'Retroactive',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 2,
    title: 'One of the characteristics of criminal law is generality. Which of the following is not an exception to the principle of generality.',
    choices: [{
        choice: 'Treaty Stipulation',
        checked: false
    }, {
        choice: 'Laws of Preferential Application',
        checked: false
    }, {
        choice: 'Principles of Public International Law',
        checked: false
    }, {
        choice: 'None of the choices',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 1,
    title: 'One of the following person is not immune from Philippine criminal law.',
    choices: [{
        choice: 'Sovereigns and other chief of state',
        checked: false
    }, {
        choice: 'Ambassador',
        checked: false
    }, {
        choice: 'Consuls',
        checked: false
    }, {
        choice: 'Charges d Affaires',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 1,
    title: 'Penal laws of the Philippines are enforceable only within its territory. This characteristic of criminal law is known as',
    choices: [{
        choice: 'General',
        checked: false
    }, {
        choice: 'Territorial',
        checked: false
    }, {
        choice: 'Prospective',
        checked: false
    }, {
        choice: 'None of these',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 2,
    title: 'Criminal law does not have any retroactive effect. This characteristic of criminal law is known as',
    choices: [{
        choice: 'General',
        checked: false
    }, {
        choice: 'Territorial',
        checked: false
    }, {
        choice: 'Prospective',
        checked: false
    }, {
        choice: 'Retroactive',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 2,
    title: 'When the law is favorable to the accused, is an exception to which characteristic of criminal law.',
    choices: [{
        choice: 'General',
        checked: false
    }, {
        choice: 'Territorial',
        checked: false
    }, {
        choice: 'Prospective',
        checked: false
    }, {
        choice: 'Retroactive',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 0,
    title: 'A Theory of criminal law, Basis is man free will to choose between good and evil. The purpose of penalty is retribution.',
    choices: [{
        choice: 'Classical Theory',
        checked: false
    }, {
        choice: 'Positivist Theory',
        checked: false
    }, {
        choice: 'Mixed Theory',
        checked: false
    }, {
        choice: 'None of these',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 2,
    title: 'The SC ruled the illegally obtained evidence is inadmissible in state criminal prosecutions in the famous case of',
    choices: [{
        choice: 'Miranda vs Arizona',
        checked: false
    }, {
        choice: 'Otit vs Jeff',
        checked: false
    }, {
        choice: 'Mapp vs Ohio',
        checked: false
    }, {
        choice: 'Milkey vs Wett',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 0,
    title: 'What is the Latin term for criminal intent?',
    choices: [{
        choice: 'Mens Rea',
        checked: false
    }, {
        choice: 'Magna Culpa',
        checked: false
    }, {
        choice: 'Inflagrante Delicto',
        checked: false
    }, {
        choice: 'Mala Vise',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 2,
    title: 'What should be conducted in order to determine whether a case falls under the jurisdiction of the regional Trial Court?',
    choices: [{
        choice: 'Inquest proceeding',
        checked: false
    }, {
        choice: 'Preliminary conference',
        checked: false
    }, {
        choice: 'Preliminary investigation',
        checked: false
    }, {
        choice: 'Search and Seizure',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 3,
    title: 'The place of trial for a criminal action is cited',
    choices: [{
        choice: 'territory',
        checked: false
    }, {
        choice: 'action',
        checked: false
    }, {
        choice: 'jurisdiction',
        checked: false
    }, {
        choice: 'venue',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 3,
    title: 'The primary purpose of bail is',
    choices: [{
        choice: 'to protect the accused rights',
        checked: false
    }, {
        choice: 'to keep the accused in jail until trial',
        checked: false
    }, {
        choice: 'to punish the accused for the crime',
        checked: false
    }, {
        choice: 'to release the accused',
        checked: false
    }]
}, {
    topicId: 3,
    ans: 0,
    title: 'The authority of the court to take cognizance of a case in the first instance is known as:',
    choices: [{
        choice: 'original jurisdiction',
        checked: false
    }, {
        choice: 'appellate jurisdiction',
        checked: false
    }, {
        choice: 'general jurisdiction',
        checked: false
    }, {
        choice: 'delegated jurisdiction',
        checked: false
    }]
}
];

crimapp.Values.value('Topic3', {
    quiz: data
});
