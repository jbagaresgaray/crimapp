var data = [{
    topicId: 1,
    ans: 1,
    title: ' It is the authority of the President of the Philippines to suspend the execution of a penalty, reduce the sentence and extinguish criminal liability.',
    choices: [{
        choice: 'Parole',
        checked: false
    }, {
        choice: 'Executive clemency',
        checked: false
    }, {
        choice: 'Pardon',
        checked: false
    }, {
        choice: 'President’s clemency',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 2,
    title: 'The B.J.M.P. is under the administration of the:',
    choices: [{
        choice: 'Executive Department',
        checked: false
    }, {
        choice: ' P.N.P.',
        checked: false
    }, {
        choice: 'D.I.L.G.',
        checked: false
    }, {
        choice: 'D.O.J',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 0,
    title: 'There are three (3) casework techniques applied by the parole officer, which is not included?',
    choices: [{
        choice: 'The trick and treat techniques',
        checked: false
    }, {
        choice: 'The executive techniques',
        checked: false
    }, {
        choice: 'The guidance, counseling and leadership techniques',
        checked: false
    }, {
        choice: 'The manipulative techniques',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 1,
    title: 'The basis of this old school of penology is the human free-will.',
    choices: [{
        choice: 'Penology School',
        checked: false
    }, {
        choice: 'Classical School',
        checked: false
    }, {
        choice: 'Neo-classical',
        checked: false
    }, {
        choice: 'Positivist',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 3,
    title: 'This helps the prisoner/detainee in the resolution of his problems',
    choices: [{
        choice: 'Meeting',
        checked: false
    }, {
        choice: 'Working',
        checked: false
    }, {
        choice: 'Recreation',
        checked: false
    }, {
        choice: 'Counseling',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 0,
    title: 'Takes charge of financial matters especially in programming, budgeting, accounting, and other activities related to financial services.  It consolidates and prepares financial reports and related statements of subsistence outlays and disbursements in the operational of the jail.',
    choices: [{
        choice: 'Budget and finance branch',
        checked: false
    }, {
        choice: 'General services branch',
        checked: false
    }, {
        choice: 'Property and supply branch',
        checked: false
    }, {
        choice: 'Mess services branch',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 3,
    title: 'Operation conducted by the BJMP wherein a prisoner maybe checked at any time.  His bedding\'s, lockers and personal belongings may also be opened at anytime, in his presence, whenever possible.  This practice is known as:',
    choices: [{
        choice: 'Check and balance',
        checked: false
    }, {
        choice: 'S.O.P.',
        checked: false
    }, {
        choice: 'Inventory',
        checked: false
    }, {
        choice: 'Operation Greyhound',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 3,
    title: 'Pardon cannot be extended to one of the following instances.',
    choices: [{
        choice: 'Murder',
        checked: false
    }, {
        choice: 'Brigandage',
        checked: false
    }, {
        choice: 'Rape',
        checked: false
    }, {
        choice: 'Impeachment',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 2,
    title: 'It refers to commission of another crime during service of sentence of penalty imposed for another previous offense.',
    choices: [{
        choice: 'Recidivism',
        checked: false
    }, {
        choice: 'Delinquency',
        checked: false
    }, {
        choice: 'Quasi-recidivism',
        checked: false
    }, {
        choice: 'City prisoner',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 0,
    title: 'A person who is detained for the violation of law or ordinance and has not been convicted is a -',
    choices: [{
        choice: 'Detention Prisoner',
        checked: false
    }, {
        choice: 'Provincial Prisoner',
        checked: false
    }, {
        choice: 'Municipal Prisoner',
        checked: false
    }, {
        choice: 'City Prisoner',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 2,
    title: 'The following are forms of executive clemency, EXCEPT',
    choices: [{
        choice: 'Commutation',
        checked: false
    }, {
        choice: 'Reform model',
        checked: false
    }, {
        choice: 'Amnesty',
        checked: false
    }, {
        choice: 'Pardon',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 1,
    title: 'It is that branch of the administration of Criminal Justice System charged with the responsibility for the custody, supervision, and rehabilitation of the convicted offender.',
    choices: [{
        choice: 'conviction',
        checked: false
    }, {
        choice: 'corrections',
        checked: false
    }, {
        choice: 'penalty',
        checked: false
    }, {
        choice: 'punishment',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 1,
    title: 'Which of the following instances Pardon cannot be exercised?',
    choices: [{
        choice: 'before conviction',
        checked: false
    }, {
        choice: 'before trial',
        checked: false
    }, {
        choice: 'after conviction',
        checked: false
    }, {
        choice: 'during service of sentence',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 3,
    title: 'This is a procedure which permits a jail prisoner to pursue his normal job during the week and return to the jail to serve his sentence during the weekend or non-working hours.',
    choices: [{
        choice: 'Amnesty',
        checked: false
    }, {
        choice: 'good conduct time allowance',
        checked: false
    }, {
        choice: 'probation',
        checked: false
    }, {
        choice: 'delayed sentence',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 2,
    title: 'The following are the justifications of punishment, EXCEPT',
    choices: [{
        choice: 'Retribution',
        checked: false
    }, {
        choice: 'Deterrence',
        checked: false
    }, {
        choice: 'Redress',
        checked: false
    }, {
        choice: 'Expiration or atonement',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 0,
    title: 'Pardon is exercised when the person is ______________',
    choices: [{
        choice: 'already convicted',
        checked: false
    }, {
        choice: 'not yet convicted',
        checked: false
    }, {
        choice: 'about to be convicted',
        checked: false
    }, {
        choice: 'serve the sentence',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 1,
    title: 'The idea that punishment will be give the offender lesson by showing to others what would happen to them if they have committed   the heinous crime.',
    choices: [{
        choice: 'Protection',
        checked: false
    }, {
        choice: 'Deterrence',
        checked: false
    }, {
        choice: 'Lethal injection',
        checked: false
    }, {
        choice: 'Stoning',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 1,
    title: 'For a convicted offender, probation is a form of ______________.',
    choices: [{
        choice: 'Punishment',
        checked: false
    }, {
        choice: 'Treatment',
        checked: false
    }, {
        choice: 'Enjoyment',
        checked: false
    }, {
        choice: 'Incarceration',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 3,
    title: 'For amnesty to be granted, there should be ______________.',
    choices: [{
        choice: 'Recommendation from U.N.',
        checked: false
    }, {
        choice: 'Recommendation from C.H.R.',
        checked: false
    }, {
        choice: 'Application from C.H.R',
        checked: false
    }, {
        choice: 'Concurrence of the congress',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 0,
    title: 'The head of the Bureau of Corrections is the',
    choices: [{
        choice: 'Director',
        checked: false
    }, {
        choice: 'Secretary of the DND',
        checked: false
    }, {
        choice: 'Chief of Executive',
        checked: false
    }, {
        choice: 'Prison Inspector',
        checked: false
    }]
}, {
    topicId: 1,
    ans: 3,
    title: 'Which program plays a unique role in the moral and spiritual regeneration of the prisoner?',
    choices: [{
        choice: 'None of these',
        checked: false
    }, {
        choice: 'Work programs',
        checked: false
    }, {
        choice: 'Education programs',
        checked: false
    }, {
        choice: 'Religious programs',
        checked: false
    }]
}];

crimapp.Values.value('Topic1', {
    quiz: data
});
