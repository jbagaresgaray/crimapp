var data = [{
    topicId: 4,
    ans: 0,
    title: 'The strict code of conduct that governs the behavior of the Mafia members is called ___.',
    choices: [{
        choice: 'Omerta',
        checked: false
    }, {
        choice: 'Triad',
        checked: false
    }, {
        choice: 'Silencer',
        checked: false
    }, {
        choice: 'Mafioso',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 0,
    title: 'The groups of crimes categorized as violent crimes (Index crimes) and property crimes (Non Index crimes) are called ___.',
    choices: [{
        choice: 'Conventional crimes',
        checked: false
    }, {
        choice: 'Non-conventional Crimes',
        checked: false
    }, {
        choice: 'Felony',
        checked: false
    }, {
        choice: 'Offense',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 0,
    title: 'What is the literal meaning of the term Cosa Nostra?',
    choices: [{
        choice: 'This thing of ours',
        checked: false
    }, {
        choice: 'Omerta',
        checked: false
    }, {
        choice: 'Two Things',
        checked: false
    }, {
        choice: '5th estate',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 0,
    title: 'The criminal activity by an enduring structure or organization developed and devoted primarily to the pursuit of profits through  illegal means commonly known as ___.',
    choices: [{
        choice: 'Organized crime',
        checked: false
    }, {
        choice: 'Professional Organization',
        checked: false
    }, {
        choice: 'White collar crime',
        checked: false
    }, {
        choice: 'Blue collar crime',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 0,
    title: 'One of the following represents the earliest codification of the Roman law, which was incorporated into the Justinian Code.',
    choices: [{
        choice: '12 Tables',
        checked: false
    }, {
        choice: 'Burgundian Code',
        checked: false
    }, {
        choice: 'Hammurabic Code',
        checked: false
    }, {
        choice: 'Code of Draco',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 0,
    title: 'The Italian leader of the positivist school of criminology, who was criticized for his methodology and his attention to the biological characteristics of offenders, was:',
    choices: [{
        choice: 'C Lombroso',
        checked: false
    }, {
        choice: 'C Beccaria',
        checked: false
    }, {
        choice: 'C Darwin',
        checked: false
    }, {
        choice: 'C Goring',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 3,
    title: 'In as much as crime is a societal creation and that it exist in a society, its study must be considered a part of social science. This means that criminology is __.',
    choices: [{
        choice: 'Applied science',
        checked: false
    }, {
        choice: 'Social Science',
        checked: false
    }, {
        choice: 'Natural Science',
        checked: false
    }, {
        choice: 'All of these',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 0,
    title: 'The term white- collar crime was coined by',
    choices: [{
        choice: 'E. Sutherland',
        checked: false
    }, {
        choice: 'R. Quinney',
        checked: false
    }, {
        choice: 'E. Durkheim',
        checked: false
    }, {
        choice: 'C. Darwin',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 3,
    title: 'What is means of "R" in the criminal formula?',
    choices: [{
        choice: 'Total Situation',
        checked: false
    }, {
        choice: 'Criminal Tendency',
        checked: false
    }, {
        choice: 'Temperament',
        checked: false
    }, {
        choice: 'none of these',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 0,
    title: 'His key ideas are concentrated on the principle of "Survival of the Fittest" as a behavioral science. He advocated the "Somatotyping Theory".',
    choices: [{
        choice: 'W Sheldon',
        checked: false
    }, {
        choice: 'R Merton',
        checked: false
    }, {
        choice: 'E Sutherland',
        checked: false
    }, {
        choice: 'Ivan Nye',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 2,
    title: 'When someone is tagged as criminal, he or she may reject it or accept it and go on to commit crime.',
    choices: [{
        choice: 'Rational Choice Theory',
        checked: false
    }, {
        choice: 'Control Theories',
        checked: false
    }, {
        choice: 'Labelling Theory',
        checked: false
    }, {
        choice: 'Social Disorganization Theory',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 1,
    title: 'It is the act of committing the crime.',
    choices: [{
        choice: 'Mens Rea',
        checked: false
    }, {
        choice: 'Actus Reus',
        checked: false
    }, {
        choice: 'Abberatio Ictus',
        checked: false
    }, {
        choice: 'Pro Reo',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 0,
    title: 'It is the mental knowledge of committing the crime.',
    choices: [{
        choice: 'Mens Rea',
        checked: false
    }, {
        choice: 'Actus Reus',
        checked: false
    }, {
        choice: 'Abberatio Ictus',
        checked: false
    }, {
        choice: 'Pro Reo',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 2,
    title: 'In Latin, It literally mean "Stand by things decided".',
    choices: [{
        choice: 'Abberatio Ictus',
        checked: false
    }, {
        choice: 'Pro Reo',
        checked: false
    }, {
        choice: 'Stare Decisis',
        checked: false
    }, {
        choice: 'Actus Reus',
        checked: false
    }]
}, {
    topicId: 4,
    ans: 1,
    title: 'It involves community supervision in lieu of prison.',
    choices: [{
        choice: 'Parole',
        checked: false
    }, {
        choice: 'Probation',
        checked: false
    }, {
        choice: 'Community Service',
        checked: false
    }, {
        choice: 'Incarceration',
        checked: false
    }]
}
];

crimapp.Values.value('Topic4', {
    quiz: data
});
