var data = [{
    topicId: 2,
    ans: 2,
    title: 'An extra judicial confession obtained from a suspect is admissible in a court of law if it was made in the presence of a counsel of his own choice and must be in',
    choices: [{
        choice: 'The presence of  a  fiscal',
        checked: false
    }, {
        choice: 'The presence of a police investigator',
        checked: false
    }, {
        choice: 'Writing',
        checked: false
    }, {
        choice: 'Front of a judge',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 3,
    title: 'Fiscals and Prosecutors are under the control and supervision of the',
    choices: [{
        choice: 'National Bureau of Investigation',
        checked: false
    }, {
        choice: 'Department of the Interior and Local Government',
        checked: false
    }, {
        choice: 'Supreme Court',
        checked: false
    }, {
        choice: 'Department of Justice',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 3,
    title: 'The questioning of a person in a formal and systematic way and is most often used to question criminal suspects to determine their probable guilt or innocence.',
    choices: [{
        choice: 'Inquiry',
        checked: false
    }, {
        choice: 'Interview',
        checked: false
    }, {
        choice: 'Polygraph examination',
        checked: false
    }, {
        choice: 'Interrogation',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 3,
    title: 'A form of investigation in which the investigator assume a different and unofficial identity.',
    choices: [{
        choice: 'Tailing',
        checked: false
    }, {
        choice: 'casing',
        checked: false
    }, {
        choice: 'pony tail',
        checked: false
    }, {
        choice: 'close tail',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 3,
    title: 'A type of surveillance in which extreme precautions and actions are taken in not losing the subject.',
    choices: [{
        choice: 'loose tail',
        checked: false
    }, {
        choice: 'casing',
        checked: false
    }, {
        choice: 'pony tail',
        checked: false
    }, {
        choice: 'close tail',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 0,
    title: 'A type of shadowing employed when a general impression of the subject’s habits and associates is required.',
    choices: [{
        choice: 'loose tail',
        checked: false
    }, {
        choice: 'casing',
        checked: false
    }, {
        choice: 'pony tail',
        checked: false
    }, {
        choice: 'close tail',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 2,
    title: 'A surveillance activity for the purpose of waiting the anticipated arrival of a suspect or observing his actions from a fixed location.',
    choices: [{
        choice: 'Casing',
        checked: false
    }, {
        choice: 'Tailing',
        checked: false
    }, {
        choice: 'Stake out',
        checked: false
    }, {
        choice: 'Espionage',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 0,
    title: 'An examination of an individual’s person, houses, or effects or a building, or premises with the purpose of discovering contraband or personal properties connected in a crime.',
    choices: [{
        choice: 'Search',
        checked: false
    }, {
        choice: 'Raid',
        checked: false
    }, {
        choice: 'Investigation',
        checked: false
    }, {
        choice: 'Seizure',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 0,
    title: 'A kind of evidence that tends to prove additional evidence of a different character to the same point.',
    choices: [{
        choice: 'Corroborative evidence',
        checked: false
    }, {
        choice: 'Circumstantial evidence',
        checked: false
    }, {
        choice: 'Direct evidence ',
        checked: false
    }, {
        choice: 'Real evidence',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 0,
    title: 'The process of bringing together in a logical manner all evidence collected during the investigation and present it to the prosecutor.',
    choices: [{
        choice: 'Case preparation',
        checked: false
    }, {
        choice: 'Order maintenance',
        checked: false
    }, {
        choice: 'Crime prevention',
        checked: false
    }, {
        choice: 'Public service',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 3,
    title: 'Ways and means are resorted for the purpose of trapping and capturing the law breaker during the execution of a criminal act.',
    choices: [{
        choice: 'Instigation',
        checked: false
    }, {
        choice: 'Inducement',
        checked: false
    }, {
        choice: 'Buy bust operation',
        checked: false
    }, {
        choice: 'Entrapment',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 3,
    title: 'A special qualification for an undercover agent.',
    choices: [{
        choice: 'excellent built',
        checked: false
    }, {
        choice: 'excellent eyesight',
        checked: false
    }, {
        choice: 'excellent looks',
        checked: false
    }, {
        choice: 'excellent memory',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 3,
    title: 'The discreet observation of places, persons and vehicles for the purpose of obtaining information concerning the identities or activities of suspects.',
    choices: [{
        choice: 'close observation',
        checked: false
    }, {
        choice: 'espionage',
        checked: false
    }, {
        choice: 'tailing',
        checked: false
    }, {
        choice: 'surveillance',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 1,
    title: 'The questioning of a person by law enforcement officers after that person has been taken into custody.',
    choices: [{
        choice: 'preliminary investigation',
        checked: false
    }, {
        choice: 'interrogation',
        checked: false
    }, {
        choice: 'custodial investigation',
        checked: false
    }, {
        choice: 'cross examination',
        checked: false
    }]
}, {
    topicId: 2,
    ans: 2,
    title: 'As a general rule, a warrant of arrest can be served at',
    choices: [{
        choice: 'day time',
        checked: false
    }, {
        choice: 'night time',
        checked: false
    }, {
        choice: 'any day and at any time of the day or night',
        checked: false
    }, {
        choice: 'weekdays',
        checked: false
    }]
}
];

crimapp.Values.value('Topic2', {
    quiz: data
});
