var data = [{
    topicId: 6,
    ans: 0,
    title: 'A primary subdivision of a bureau with a department wide responsibility for providing a specific specialized functions.',
    choices: [{
        choice: 'Section',
        checked: false
    }, {
        choice: 'Sector',
        checked: false
    }, {
        choice: 'Squad',
        checked: false
    }, {
        choice: 'Detail',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 2,
    title: 'A subdivision of a squad',
    choices: [{
        choice: 'Section',
        checked: false
    }, {
        choice: 'Unit',
        checked: false
    }, {
        choice: 'Sector',
        checked: false
    }, {
        choice: 'Detail',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 2,
    title: 'A subdivision of a unit.',
    choices: [{
        choice: 'Section',
        checked: false
    }, {
        choice: 'Unit',
        checked: false
    }, {
        choice: 'Squad',
        checked: false
    }, {
        choice: 'Detail',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 3,
    title: 'A subdivision of a section.',
    choices: [{
        choice: 'Precinct',
        checked: false
    }, {
        choice: 'Unit',
        checked: false
    }, {
        choice: 'Squad',
        checked: false
    }, {
        choice: 'Detail',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 1,
    title: 'The primary geographic subdivision of a precinct.',
    choices: [{
        choice: 'Post',
        checked: false
    }, {
        choice: 'Sector',
        checked: false
    }, {
        choice: 'Section',
        checked: false
    }, {
        choice: 'Unit',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 1,
    title: 'The primary subdivision of a sector.',
    choices: [{
        choice: 'Post',
        checked: false
    }, {
        choice: 'Sector',
        checked: false
    }, {
        choice: 'Section',
        checked: false
    }, {
        choice: 'Unit',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 2,
    title: 'One of several tours of duty.',
    choices: [{
        choice: 'Detail',
        checked: false
    }, {
        choice: 'Post',
        checked: false
    }, {
        choice: 'Shift',
        checked: false
    }, {
        choice: 'Beat',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 0,
    title: 'Fixed geographic location usually assigned to an individual officer.',
    choices: [{
        choice: 'Post',
        checked: false
    }, {
        choice: 'Beat',
        checked: false
    }, {
        choice: 'Shift',
        checked: false
    }, {
        choice: 'Section',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 1,
    title: 'The primary geographic subdivision of the patrol operation bureau.',
    choices: [{
        choice: 'Precinct',
        checked: false
    }, {
        choice: 'Section',
        checked: false
    }, {
        choice: 'Sector',
        checked: false
    }, {
        choice: 'Unit',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 3,
    title: 'It means planning the work of the department and of the personnel in an orderly manner.',
    choices: [{
        choice: 'Plan',
        checked: false
    }, {
        choice: 'Delegate',
        checked: false
    }, {
        choice: 'Oversee',
        checked: false
    }, {
        choice: 'Organize',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 1,
    title: 'Sir Robert Peel introduced the Metropolitan Police Act and passed by the parliament of England on.',
    choices: [{
        choice: '1828',
        checked: false
    }, {
        choice: '1829',
        checked: false
    }, {
        choice: '1830',
        checked: false
    }, {
        choice: '1831',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 0,
    title: 'Considered as the father of modern policing system.',
    choices: [{
        choice: 'Sir Robert Peel',
        checked: false
    }, {
        choice: 'August Vollmer',
        checked: false
    }, {
        choice: 'Edgar Hoover',
        checked: false
    }, {
        choice: 'Henry Armstrong',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 0,
    title: 'In the principles of law enforcement enunciated by Sir Robert Peel, the basic mission of the police is',
    choices: [{
        choice: 'Prevention of crime',
        checked: false
    }, {
        choice: 'Enforcement of the law',
        checked: false
    }, {
        choice: 'Maintenance of peace and order',
        checked: false
    }, {
        choice: 'None of these',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 0,
    title: 'One of the feature of this act is that no freeman shall be taken or imprisoned except by the judgment of his peer',
    choices: [{
        choice: 'Magna Carta',
        checked: false
    }, {
        choice: 'Statute of 1295',
        checked: false
    }, {
        choice: 'Legies Henry',
        checked: false
    }, {
        choice: 'None of these',
        checked: false
    }]
}, {
    topicId: 6,
    ans: 0,
    title: 'The closing of gates of London during sun down. This mark the beginning of the curfew hours',
    choices: [{
        choice: 'Statute of 1295',
        checked: false
    }, {
        choice: 'Statute of 1775',
        checked: false
    }, {
        choice: 'Statute of 1827',
        checked: false
    }, {
        choice: 'Statute of 1834',
        checked: false
    }]
}
];

crimapp.Values.value('Topic6', {
    quiz: data
});
