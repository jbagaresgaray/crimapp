 var crimapp = crimapp || {};

 crimapp.Controllers = angular.module('crimapp.controllers', []);
 crimapp.Services = angular.module('crimapp.services', []);
 crimapp.Directive = angular.module('crimapp.directive', []);
 crimapp.Values = angular.module('crimapp.values', []);


 angular.module('crimapp', ['ionic', 'ngCordova', 'ngSanitize', 'crimapp.controllers', 'crimapp.services', 'crimapp.values'])

 .run(function($ionicPlatform, $rootScope, $ionicHistory) {
     $ionicPlatform.ready(function() {
         // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
         // for form inputs)
         if (window.cordova && window.cordova.plugins.Keyboard) {
             cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
         }
         if (window.StatusBar) {
             // org.apache.cordova.statusbar required
             StatusBar.styleDefault();
         }
     });
     $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
         // $ionicHistory.clearCache();
     });
 })
     .run(function(DB) {
         DB.init();
     })
     .constant('DB_CONFIG', {
         name: 'DB',
         tables: [{
             name: 'scores',
             columns: [{
                 name: 'id',
                 type: 'integer primary key AUTOINCREMENT'
             }, {
                 name: 'topicId',
                 type: 'text'
             }, {
                 name: 'name',
                 type: 'text'
             }, {
                 name: 'score',
                 type: 'text'
             }]
         }]
     })
     .config(function($stateProvider, $urlRouterProvider) {
         $stateProvider
             .state('intro', {
                 url: '/',
                 templateUrl: 'templates/intro.html',
                 controller: 'IntroCtrl'
             })
             .state('quiz', {
                 url: "/quiz",
                 templateUrl: "templates/quiz.html",
                 controller: 'QuizCtrl',
             })
             .state('score', {
                 url: "/score/:topicId",
                 templateUrl: "templates/score.html",
                 controller: 'ScoreCtrl'
             })
             .state('instruction', {
                 url: '/instruction',
                 templateUrl: 'templates/instruction.html',
                 controller: 'IntroCtrl'
             })
             .state('app', {
                 url: "/app",
                 abstract: true,
                 templateUrl: "templates/menu.html",
                 controller: 'AppCtrl'
             })
             .state('app.author', {
                 url: "/author",
                 views: {
                     'menuContent': {
                         templateUrl: "templates/author.html"
                     }
                 }
             })
             .state('app.about', {
                 url: "/about",
                 views: {
                     'menuContent': {
                         templateUrl: "templates/about.html"
                     }
                 }
             })
             .state('app.topics', {
                 url: "/topics",
                 views: {
                     'menuContent': {
                         templateUrl: "templates/topics.html",
                         controller: 'LessonCtrl',
                         resolve: {
                             topics: function(TopicFactory) {
                                 return TopicFactory.getTopics();
                             }
                         }
                     }
                 }
             })
             .state('app.single', {
                 url: "/topics/:topicId/:title",
                 views: {
                     'menuContent': {
                         templateUrl: "templates/lesson.html",
                         controller: 'LessonListCtrl',
                         resolve: {
                             lessons: function(LessonFactory) {
                                 return LessonFactory.getLessons();
                             }
                         }
                     }
                 }
             })
             .state('app.lesson', {
                 url: "/lesson/:lessonId/:title",
                 views: {
                     'menuContent': {
                         templateUrl: "templates/lessondetail.html",
                         controller: 'LessonDetailCtrl',
                         resolve: {
                             details: function(LessondDetailFactory) {
                                 return LessondDetailFactory.getLessons() || [];
                             }
                         }
                     }
                 }
             });
         // if none of the above states are matched, use this as the fallback
         $urlRouterProvider.otherwise('/');
     });
