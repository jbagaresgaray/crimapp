'use strict';

var score;
var questionmade = [];
var questions = [];
var questionnumber;

function shuffle(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i]
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}


var QuizCtrl = function($scope, $rootScope, $location, $state, $timeout, $ionicLoading, Quiz1Factory, Quiz2Factory, Quiz3Factory, Quiz4Factory, Quiz5Factory, Quiz6Factory) {

    init();

    $scope.serverSideChange = function(index, item, scope) {
        $scope.data = {
            index: index,
            value: item.choice,
            scope: scope
        }
        $scope.enable = true;
    };

    $scope.nextquestion = function() {
        $ionicLoading.show({
            template: 'Loading...'
        });

        $timeout(function() {
            var len = questions.length;
            if (questionnumber != (len - 1)) {
                questionnumber = questionnumber + 1;

                questionmade.push($scope.data);

                $scope.questions = questions[questionnumber];

                $scope.enable = false;
                $scope.prevenable = true;
            } else {
                var result = {
                    topicId: $scope.id
                };

                questionmade.push($scope.data);

                $scope.enable = false;
                $scope.prevenable = false;

                window.localStorage['quiz'] = JSON.stringify(questionmade);

                $state.go('score', result);
            }

            $ionicLoading.hide();
        }, 300);
    }

    $scope.home = function() {
        $state.go('app.topics');
    }

    $scope.previousquestion = function() {
        $ionicLoading.show({
            template: 'Loading...'
        });

        $timeout(function() {
            if (questionnumber != 0) {
                questionnumber = questionnumber - 1;
                $scope.questions = questions[questionnumber];
                $scope.prevenable = true;
                $scope.enable = true;
            } else {
                $scope.prevenable = false;
                $scope.enable = true;
            }
            $ionicLoading.hide();
        }, 300);
    }


    function init() {

        $scope.id = window.localStorage['quizId'];
        $scope.title = window.localStorage['quizTitle'];

        questionnumber = 0;
        $scope.enable = false;
        $scope.prevenable = false;

        if ($scope.id == 1) {
            questions = Quiz1Factory.getQuiz();
            console.log('question 1: ', questions);
        } else if ($scope.id == 2) {
            questions = Quiz2Factory.getQuiz();
            console.log('question 2: ', questions);
        } else if ($scope.id == 3) {
            questions = Quiz3Factory.getQuiz();
            console.log('question 3: ', questions);
        } else if ($scope.id == 4) {
            questions = Quiz4Factory.getQuiz();
            console.log('question 4: ', questions);
        } else if ($scope.id == 5) {
            questions = Quiz5Factory.getQuiz();
            console.log('question 5: ', questions);
        } else if ($scope.id == 6) {
            questions = Quiz6Factory.getQuiz();
            console.log('question 6: ', questions);
        }

        questions = shuffle(questions);

        $ionicLoading.show({
            template: 'Loading...'
        });

        $timeout(function() {
            $scope.questions = questions[questionnumber];

            $ionicLoading.hide();
        }, 1000);

        $scope.data = {
            index: '',
            value: '',
            scope: {}
        };
    }

}


var ScoreCtrl = function($scope, $timeout, $ionicHistory, $stateParams, $ionicPopup, $state, $location, $window, Score) {
    $scope.id = $stateParams.topicId;

    $scope.quiz = {
        title: '',
        answer: '',
        myans: ''
    };
    var quizarray = [];
    $scope.results = {};
    $scope.score = {};
    $scope.data = {}

    Score.previousScore($scope.id).then(function(documents) {
        $scope.previousScore = documents;
    });

    var quiz = JSON.parse(window.localStorage['quiz'] || {});
    var score = 0;
    for (var i = 0; i < quiz.length; i++) {
        var myans = quiz[i].index;
        var mychoice = quiz[i].scope.choices[myans].choice;

        var ans = quiz[i].scope.ans;
        var title = quiz[i].scope.title;
        var choice = quiz[i].scope.choices[ans].choice;

        var iscorrect = false;
        if (myans == ans) {
            iscorrect = true;
            score = score + 1;
        }

        $scope.quiz = {
            title: title,
            answer: choice,
            myans: mychoice,
            correct: iscorrect
        };
        quizarray.push($scope.quiz);

    };
    $scope.results = quizarray;
    $scope.score = score;
    $scope.limit = quizarray.length;

    $scope.resetQuiz = function() {
        $state.go('quiz', {}, {
            reload: true
        });
        $timeout(function() {
            window.location.reload();
        }, 20);

        // $ionicHistory.clearHistory();
        // $ionicHistory.clearCache();
    }

    $scope.saveScore = function() {
        $ionicPopup.show({
            title: 'Save Score',
            template: '<input type="text" ng-model="data.username">',
            subTitle: 'Please Enter your name',
            scope: $scope,
            buttons: [{
                text: 'Cancel'
            }, {
                text: '<b>Save</b>',
                type: 'button-positive',
                onTap: function(e) {
                    if (!$scope.data.username) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Crimilogy App',
                            template: 'Please enter your name'
                        });
                        return;
                    } else {
                        var score = $scope.score + '/' + quizarray.length;
                        Score.insert($scope.id, $scope.data.username, score).then(function(documents) {
                            console.log(documents);
                            if (documents) {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Crimilogy App',
                                    template: 'Record Successfully Saved'
                                });
                                $state.go('app.topics');
                            }
                        });
                    }
                }
            }]
        });
    }
}

crimapp.Controllers.controller('ScoreCtrl', ['$scope', '$timeout', '$ionicHistory', '$stateParams', '$ionicPopup', '$state', '$location', '$window', 'Score', ScoreCtrl]);
crimapp.Controllers.controller('QuizCtrl', ['$scope', '$rootScope', '$location', '$state', '$timeout', '$ionicLoading', 'Quiz1Factory', 'Quiz2Factory', 'Quiz3Factory', 'Quiz4Factory', 'Quiz5Factory', 'Quiz6Factory', QuizCtrl]);
