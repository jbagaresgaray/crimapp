var LessonListCtrl = function($scope, $ionicModal, $state, $stateParams, $location, $ionicLoading, $timeout, Score, lessons) {

    init();


    $ionicModal.fromTemplateUrl('templates/scoreboard.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.scoreboardmodal = modal;
    });

    $scope.closescoreboard = function() {
        $scope.scoreboardmodal.hide();
    };

    $scope.openscoreboard = function() {

        $scope.scoreboardmodal.show();

        $ionicLoading.show({
            template: 'Loading...'
        });

        $timeout(function() {
            Score.byTopic($scope.id).then(function(document) {
                $scope.scores = document;
            });
            $ionicLoading.hide();
        }, 2000);
    };

    $scope.refreshScore = function() {
        $ionicLoading.show({
            template: 'Loading...'
        });

        $timeout(function() {
            Score.byTopic($scope.id).then(function(document) {
                $scope.scores = document;
            });
            $ionicLoading.hide();
        }, 2000);
    }

    $scope.startQuiz = function(id, title) {
        window.localStorage['quizId'] = id;
        window.localStorage['quizTitle'] = title;

        $state.go('quiz');
        $timeout(function() {
            window.location.reload();
        }, 20);
    }

    function init() {

        $scope.title = $stateParams.title;
        $scope.id = $stateParams.topicId;
        $scope.scores = {};

        $scope.lessons = lessons;
    }
}

var LessonCtrl = function($scope, $state, topics) {
    $scope.topics = topics;
}



var LessonDetailCtrl = function($scope, $stateParams, $filter, $sce, details) {
    $scope.title = $stateParams.title;
    $scope.id = $stateParams.lessonId;

    $scope.lessons = details;

    $scope.detail = $filter('filter')($scope.lessons, {
        id: $scope.id
    })[0];

    $scope.currentProjectUrl = $sce.trustAsResourceUrl('js/JSON/lesson/' + $scope.detail.src);

    $(document).ready(function() {
        $('#foo').on('load', function() {
            $('#loadingMessage').hide();
            $('#loadingMessage').css('display', 'none');
        });
    });
}

crimapp.Controllers.controller('LessonCtrl', ['$scope', '$state', 'topics', LessonCtrl]);
crimapp.Controllers.controller('LessonListCtrl', ['$scope', '$ionicModal', '$state', '$stateParams', '$location', '$ionicLoading', '$timeout', 'Score', 'lessons', LessonListCtrl]);
crimapp.Controllers.controller('LessonDetailCtrl', ['$scope', '$stateParams', '$filter', '$sce', 'details', LessonDetailCtrl]);
