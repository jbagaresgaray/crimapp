'use strict';

var TopicFactory = function($http, Topics) {
    var TopicFactory = {};
    TopicFactory.topics = [];

    TopicFactory.getTopics = function() {
        TopicFactory.topics = Topics.topics
        return TopicFactory.topics;
    };

    return TopicFactory;
};

var LessonFactory = function($http, Lessons) {
    var LessonFactory = {};
    LessonFactory.lessons = [];

    LessonFactory.getLessons = function() {
        LessonFactory.lessons = Lessons.lessons
        return LessonFactory.lessons;
    };

    return LessonFactory;
};

var LessondDetailFactory = function($http, LessonDetail) {
    var LessondDetailFactory = {};
    LessondDetailFactory.lessons = [];

    LessondDetailFactory.getLessons = function() {
        LessondDetailFactory.lessons = LessonDetail.lessons
        return LessondDetailFactory.lessons;
    };

    return LessondDetailFactory;
};

var Quiz1Factory = function($http, Topic1) {
    var Quiz1Factory = {};
    Quiz1Factory.quiz = [];

    Quiz1Factory.getQuiz = function() {
        Quiz1Factory.quiz = Topic1.quiz
        return Quiz1Factory.quiz;
    };

    return Quiz1Factory;
};

var Quiz2Factory = function($http, Topic2) {
    var Quiz2Factory = {};
    Quiz2Factory.quiz = [];

    Quiz2Factory.getQuiz = function() {
        Quiz2Factory.quiz = Topic2.quiz
        return Quiz2Factory.quiz;
    };

    return Quiz2Factory;
};

var Quiz3Factory = function($http, Topic3) {
    var QuizFactory = {};
    QuizFactory.quiz = [];

    QuizFactory.getQuiz = function() {
        QuizFactory.quiz = Topic3.quiz
        return QuizFactory.quiz;
    };

    return QuizFactory;
};

var Quiz4Factory = function($http, Topic4) {
    var QuizFactory = {};
    QuizFactory.quiz = [];

    QuizFactory.getQuiz = function() {
        QuizFactory.quiz = Topic4.quiz
        return QuizFactory.quiz;
    };

    return QuizFactory;
};

var Quiz5Factory = function($http, Topic5) {
    var QuizFactory = {};
    QuizFactory.quiz = [];

    QuizFactory.getQuiz = function() {
        QuizFactory.quiz = Topic5.quiz
        return QuizFactory.quiz;
    };

    return QuizFactory;
};

var Quiz6Factory = function($http, Topic6) {
    var QuizFactory = {};
    QuizFactory.quiz = [];

    QuizFactory.getQuiz = function() {
        QuizFactory.quiz = Topic6.quiz
        return QuizFactory.quiz;
    };

    return QuizFactory;
};

var Score = function(DB) {
    var self = this;

    self.all = function() {
        return DB.query('SELECT * FROM scores')
            .then(function(result) {
                return DB.fetchAll(result);
            });
    };

    self.byTopic = function(topicId) {
        return DB.query('SELECT * FROM scores WHERE topicId=? ORDER BY score DESC;', [topicId])
            .then(function(result) {
                return DB.fetchAll(result);
            });
    };

    self.previousScore = function(topicId) {
        return DB.query('SELECT * FROM scores WHERE topicId=? ORDER BY id DESC LIMIT 1;', [topicId])
            .then(function(result) {
                return DB.fetch(result);
            });
    };

    self.insert = function(topicId, name, score) {
        return DB.query('INSERT INTO scores(topicId,name,score) VALUES(?,?,?)', [topicId, name, score])
            .then(function(result) {
                return result.insertId;
            });
    }
    return self;
}

crimapp.Services.factory('Score', ['DB', Score]);
crimapp.Services.factory('TopicFactory', ['$http', 'Topics', TopicFactory]);
crimapp.Services.factory('LessonFactory', ['$http', 'Lessons', LessonFactory]);
crimapp.Services.factory('Quiz1Factory', ['$http', 'Topic1', Quiz1Factory]);
crimapp.Services.factory('Quiz2Factory', ['$http', 'Topic2', Quiz2Factory]);
crimapp.Services.factory('Quiz3Factory', ['$http', 'Topic3', Quiz3Factory]);
crimapp.Services.factory('Quiz4Factory', ['$http', 'Topic4', Quiz4Factory]);
crimapp.Services.factory('Quiz5Factory', ['$http', 'Topic5', Quiz5Factory]);
crimapp.Services.factory('Quiz6Factory', ['$http', 'Topic6', Quiz6Factory]);
crimapp.Services.factory('LessondDetailFactory', ['$http', 'LessonDetail', LessondDetailFactory]);
